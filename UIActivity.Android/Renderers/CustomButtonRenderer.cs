﻿using System;
using Android.Content;
using UIActivity.Droid.Renderers;
using UIActivity.Utilities.Renderers;
using Xamarin.Forms;
using Xamarin.Forms.Platform.Android;

[assembly: ExportRenderer(typeof(CustomButton), typeof(CustomButtonRenderer))]
namespace UIActivity.Droid.Renderers
{
    public class CustomButtonRenderer : ButtonRenderer
    {
        public CustomButtonRenderer(Context context ): base(context)
        {

        }

        protected override void OnElementChanged(ElementChangedEventArgs<Button> e)
        {
            base.OnElementChanged(e);
            Control.SetAllCaps(false);
            Control.Gravity = Android.Views.GravityFlags.Center;
            Control.SetPadding(0, 0, 0, 1);
        }
    }
}

