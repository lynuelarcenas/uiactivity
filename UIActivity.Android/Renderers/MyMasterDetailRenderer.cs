﻿using System;
using Android.Content;
using UIActivity.Droid.Renderers;
using Xamarin.Forms;
using Xamarin.Forms.Platform.Android.AppCompat;

[assembly: ExportRenderer(typeof(Xamarin.Forms.MasterDetailPage), typeof(MyMasterDetailRenderer))]
namespace UIActivity.Droid.Renderers
{
    public class MyMasterDetailRenderer : MasterDetailPageRenderer
    {
        bool firstDone;

        public MyMasterDetailRenderer(Context context) : base(context)
        {

        }

        public override void AddView(Android.Views.View child)
        {

            if (firstDone)
            {
                LayoutParams p = (LayoutParams)child.LayoutParameters;
                p.Width = Resources.DisplayMetrics.WidthPixels;
                base.AddView(child, p);
            }
            else
            {
                firstDone = true;
                base.AddView(child);
            }

        }
    }
}
