﻿using System;

using Android.App;
using Android.Content.PM;
using Android.Runtime;
using Android.Views;
using Android.Widget;
using Android.OS;

namespace UIActivity.Droid
{
    [Activity(Label = "UIActivity", Icon = "@mipmap/icon", Theme = "@style/SplashScreen", MainLauncher = true, ConfigurationChanges = ConfigChanges.ScreenSize | ConfigChanges.Orientation)]
    public class MainActivity : global::Xamarin.Forms.Platform.Android.FormsAppCompatActivity
    {
        protected override void OnCreate(Bundle bundle)
        {
            TabLayoutResource = Resource.Layout.Tabbar;
            ToolbarResource = Resource.Layout.Toolbar;

            base.Window.RequestFeature(WindowFeatures.ActionBar);
            base.SetTheme(Resource.Style.MainTheme);

            base.OnCreate(bundle);

            this.Window.AddFlags(WindowManagerFlags.TranslucentNavigation);
            this.Window.AddFlags(WindowManagerFlags.DrawsSystemBarBackgrounds);

            var width = Resources.DisplayMetrics.WidthPixels;
            var height = Resources.DisplayMetrics.HeightPixels;
            var density = Resources.DisplayMetrics.Density;

            App.ScreenWidth = width / density;
            App.ScreenHeight = height / density;
            App.DeviceScale = density;

            global::Xamarin.Forms.Forms.Init(this, bundle);
            LoadApplication(new App());


        }
    }
}

