using System;
using UIActivity.Views;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;

[assembly: XamlCompilation(XamlCompilationOptions.Compile)]
namespace UIActivity
{
    public partial class App : Application
    {
        public static double ScreenWidth { get; set; }
        public static double ScreenHeight { get; set; }
        public static float DeviceScale { get; set; }
       public static double ScreenScale
       {
           get
           {
               if (ScreenHeight < 568)
               {
                   return (ScreenWidth + 568) / (320.0F + 568.0F);
               }
               else
               {
                   return (ScreenWidth + ScreenHeight) / (320.0F + 568.0F);
               }
           }
       }

        public App()
        {
            InitializeComponent();

            MainPage = new NavigationPage (new SignInPage()){BarTextColor = Color.White};

            Console.WriteLine("WIDTH: " + ScreenWidth);
            Console.WriteLine("HEIGHT: " + ScreenHeight);
            Console.WriteLine("SCALE: " + ScreenScale);
        }

        protected override void OnStart()
        {
            // Handle when your app starts
        }

        protected override void OnSleep()
        {
            // Handle when your app sleeps
        }

        protected override void OnResume()
        {
            // Handle when your app resumes
        }
    }
}
