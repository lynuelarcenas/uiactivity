﻿using System;
using System.Threading;
using Newtonsoft.Json.Linq;

namespace UIActivity.Utilities.Helpers.FileReader
{
    public interface IFileConnector
    {
        void ReceiveJSONData(JObject jsonData, CancellationToken ct);
    }
}
