﻿using System;
using System.Threading;
using System.Threading.Tasks;

namespace UIActivity.Utilities.Helpers.FileReader
{
    public interface IFileReader
    {
        Task WriteFile(string fileName, string json, bool isEmbed, CancellationToken ct);
        Task ReadFile(string fileName, bool isEmbed, CancellationToken ct);
    }
}
