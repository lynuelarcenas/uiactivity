﻿using System;
using Xamarin.Forms;

namespace UIActivity.Utilities.Behaviors
{
    public class EntryBehavior : Behavior<Entry>
    {
        protected override void OnAttachedTo(Entry entry)
        {
            entry.Focused += OnEntryFocused;
            base.OnAttachedTo(entry);
        }

        protected override void OnDetachingFrom(Entry entry)
        {
            
            entry.Focused -= OnEntryFocused;
            base.OnDetachingFrom(entry);
        }

        private void OnEntryFocused(object sender, FocusEventArgs e)
        {
            ((Entry)sender).Placeholder = "";
            ((Entry)sender).PlaceholderColor = Color.Default;
        }


    }
}
