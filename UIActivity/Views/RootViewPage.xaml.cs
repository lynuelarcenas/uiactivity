﻿using System;
using System.Collections.Generic;
using System.Windows.Input;
using Xamarin.Forms;

namespace UIActivity.Views
{
    public partial class RootViewPage : ContentPage
    {
        public static readonly BindableProperty PageTitleProperty = BindableProperty.Create("PageTitle", typeof(string), typeof(RootViewPage), null);

        public static readonly BindableProperty LeftIconProperty = BindableProperty.Create("LeftButtonIcon", typeof(string), typeof(RootViewPage), null);
        public static readonly BindableProperty RightIconProperty = BindableProperty.Create("RightButtonIcon", typeof(string), typeof(RootViewPage), null);

        public static readonly BindableProperty LeftButtonCommandProperty = BindableProperty.Create("LeftButtonCommand", typeof(ICommand), typeof(RootViewPage), null);
        public static readonly BindableProperty RightButtonCommandProperty = BindableProperty.Create("RightButtonCommand", typeof(ICommand), typeof(RootViewPage), null);

        public string PageTitle
        {
            set { SetValue(PageTitleProperty, value); }
            get { return (string)GetValue(PageTitleProperty); }
        }

        public string LeftButtonIcon
        {
            set { SetValue(LeftIconProperty, value); }
            get { return (string)GetValue(LeftIconProperty); }
        }

        public string RightButtonIcon
        {
            set { SetValue(RightIconProperty, value); }
            get { return (string)GetValue(RightIconProperty); }
        }

        public ICommand LeftButtonCommand
        {
            set { SetValue(LeftButtonCommandProperty, value); }
            get { return (ICommand)GetValue(LeftButtonCommandProperty); }
        }

        public ICommand RightButtonCommand
        {
            set { SetValue(RightButtonCommandProperty, value); }
            get { return (ICommand)GetValue(RightButtonCommandProperty); }
        }
    }
}
