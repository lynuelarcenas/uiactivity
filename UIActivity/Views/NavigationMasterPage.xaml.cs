﻿using System;
using System.Collections.Generic;

using Xamarin.Forms;

namespace UIActivity.Views
{
    public partial class NavigationMasterPage : RootViewPage
    {
        public NavigationMasterPage()
        {
            InitializeComponent();

            this.LeftButtonIcon = "CloseIcon";
            this.LeftButtonCommand = new Command((obj) => ((MasterDetailPage)((NavigationPage)Application.Current.MainPage).CurrentPage).IsPresented = false);
        }

        void HomeButton_Clicked(object sender, System.EventArgs e)
        {
            ((MasterDetailPage)((NavigationPage)Application.Current.MainPage).CurrentPage).Detail = new HomePage();
            ((MasterDetailPage)((NavigationPage)Application.Current.MainPage).CurrentPage).IsPresented = false;
        }

        void Handle_Clicked_1(object sender, System.EventArgs e)
        {
            ((MasterDetailPage)((NavigationPage)Application.Current.MainPage).CurrentPage).Detail = new ProfilePage();
            ((MasterDetailPage)((NavigationPage)Application.Current.MainPage).CurrentPage).IsPresented = false;
        }

        async void Handle_Clicked(object sender, System.EventArgs e)
        {
            bool action = await DisplayAlert("Logout", "Are you sure you want to log out?", "Yes", "No");

            if (action)
            {
                Application.Current.MainPage = new NavigationPage(new SignInPage()){BarTextColor = Color.White};
            }
            else
            {
                return;
            }

        }
    }
}
