﻿using System;
using System.Collections.Generic;

using Xamarin.Forms;

namespace UIActivity.Views
{
    public partial class ProfilePage : RootViewPage
    {
        public ProfilePage()
        {
            InitializeComponent();

            NavigationPage.SetHasNavigationBar(this, false);

            this.PageTitle = "Profile";
            this.LeftButtonIcon = "HamburgerIcon";
            this.RightButtonIcon = "EditIcon";
            this.RightButtonCommand = new Command((obj) => { usernameEntry.IsEnabled = true; emailEntry.IsEnabled = true; passwordEntry.IsEnabled = true; });
            this.LeftButtonCommand = new Command((obj) => ((MasterDetailPage)((NavigationPage)App.Current.MainPage).CurrentPage).IsPresented = true);
        }

        async void GoogleConnectButton_Clicked(object sender, System.EventArgs e)
        {
            bool action = await DisplayAlert("Google+", "Allow this App to connect to your Google+ account", "Okay", "No");
            if (action)
            {
                googleConnectButton.IsVisible = false;
                googleConnectButton.IsEnabled = false;
                googleSwitch.IsVisible = true;
                googleSwitch.IsEnabled = true;
            }
            else
            {
                return;
            }
        }
    }
}
