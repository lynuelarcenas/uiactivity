﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Threading;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using UIActivity.Models;
using UIActivity.Utilities.Helpers.FileReader;
using Xamarin.Forms;

namespace UIActivity.Views
{
    public partial class HomePage : RootViewPage, IFileConnector
    {
        FileReader fileReader = new FileReader();
        CancellationTokenSource cancellationToken = new CancellationTokenSource();

        public HomePage()
        {
            InitializeComponent();
            NavigationPage.SetHasNavigationBar(this, false);
            fileReader.FileReaderDelegate = this;
            GetData();
            InitializeNavigation();


        }

        private void InitializeNavigation()
        {
            this.PageTitle = "Newsfeed";
            this.LeftButtonIcon = "HamburgerIcon";
            this.RightButtonIcon = "SearchIcon";
            this.LeftButtonCommand = new Command((obj) => ((MasterDetailPage)((NavigationPage)App.Current.MainPage).CurrentPage).IsPresented = true);
        }

        private async void GetData()
        {
            await fileReader.ReadFile("News.json", true, cancellationToken.Token);
        }

        void AllButton_Clicked(object sender, System.EventArgs e)
        {
            firstButtonIndicator.IsVisible = true;
            secondButtonIndicator.IsVisible = false;
            thirdButtonIndicator.IsVisible = false;
            fourthButtonIndicator.IsVisible = false;
        }

        void FeaturedButton_Clicked(object sender, System.EventArgs e)
        {
            firstButtonIndicator.IsVisible = false;
            secondButtonIndicator.IsVisible = true;
            thirdButtonIndicator.IsVisible = false;
            fourthButtonIndicator.IsVisible = false;
        }

        void PopularButton_Clicked(object sender, System.EventArgs e)
        {
            firstButtonIndicator.IsVisible = false;
            secondButtonIndicator.IsVisible = false;
            thirdButtonIndicator.IsVisible = true;
            fourthButtonIndicator.IsVisible = false;
        }

        void MyFavoritesButton_Clicked(object sender, System.EventArgs e)
        {
            firstButtonIndicator.IsVisible = false;
            secondButtonIndicator.IsVisible = false;
            thirdButtonIndicator.IsVisible = false;
            fourthButtonIndicator.IsVisible = true;
        }

        public void ReceiveJSONData(JObject jsonData, CancellationToken ct)
        {
            var news = JsonConvert.DeserializeObject<ObservableCollection<Models.NewsArticle>>(jsonData["news"].ToString());
            this.listView.ItemsSource = news;
            foreach (var newsArticle in news)
            {
                Console.WriteLine(newsArticle.title);
                Console.WriteLine(newsArticle.likes);
                Console.WriteLine(newsArticle.comments);
            }
        }
    }
}
