﻿using System;
using System.Collections.Generic;

using Xamarin.Forms;

namespace UIActivity.Views
{
    public partial class SignInPage : ContentPage
    {
        public SignInPage()
        {
            InitializeComponent();
            NavigationPage.SetHasNavigationBar(this, false);
        }

        void SignInButton_Clicked(object sender, System.EventArgs e)
        {
            if (string.IsNullOrEmpty(usernameEntry.Text))
            {
                usernameEntry.Placeholder = "Required Field";
                usernameEntry.PlaceholderColor = Color.Red;
                Console.WriteLine("Username Is Empty");
            }

            if (string.IsNullOrEmpty(passwordEntry.Text))
            {
                passwordEntry.Placeholder = "Required Field";
                passwordEntry.PlaceholderColor = Color.Red;
                Console.WriteLine("Password Is Empty");
            }
            else
            {
                Application.Current.MainPage = new NavigationPage(new LandingPage()) { BarTextColor = Color.White };
            }


            //if ((string.IsNullOrEmpty(usernameEntry.Text)) && (!string.IsNullOrEmpty(passwordEntry.Text)))
            //{
            //    usernameEntry.Placeholder = "Required Field";
            //    usernameEntry.PlaceholderColor = Color.Red;
            //}

            //if ((!string.IsNullOrEmpty(usernameEntry.Text)) && (string.IsNullOrEmpty(passwordEntry.Text)))
            //{
            //    passwordEntry.Placeholder = "Required Field";
            //    passwordEntry.PlaceholderColor = Color.Red;
            //}

            //if (string.IsNullOrEmpty(passwordEntry.Text))
            //{
            //    passwordEntry.Placeholder = "Required Field";
            //    passwordEntry.PlaceholderColor = Color.Red;
            //}

            //if (string.IsNullOrEmpty(usernameEntry.Text))
            //{
            //    usernameEntry.Placeholder = "Required Field";
            //    usernameEntry.PlaceholderColor = Color.Red;
            //}

            //if ((!string.IsNullOrEmpty(usernameEntry.Text)) && (!string.IsNullOrEmpty(passwordEntry.Text)))
            //{
            //    Application.Current.MainPage = new NavigationPage( new LandingPage()){BarTextColor = Color.White};

            //}

            //else
            //{
            //    passwordEntry.Placeholder = "";
            //    usernameEntry.Placeholder = "";
            //}
        }
    }
}
