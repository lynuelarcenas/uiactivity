﻿using System;
using System.Collections.ObjectModel;

namespace UIActivity.Models
{
    public class User
    {
        public static User dataClass;
        public static User getInstance
        {
            get
            {
                if (dataClass == null)
                {
                    dataClass = new User();
                }

                return dataClass;
            }
        }

        public string username { get; set; }
        public string password { get; set; }
        public string email { get; set; }
        public string profile_picture { get; set; }
        public string profile_cover { get; set; }
        public int facebook_followers { get; set; }
        public int twitter_followers { get; set; }
        public int is_facebook { get; set; }
        public int is_twitter { get; set; }
        public int is_google { get; set; }

        public static ObservableCollection<User> users = new ObservableCollection<User>();
    }
}
