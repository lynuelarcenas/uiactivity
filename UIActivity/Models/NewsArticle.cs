﻿using System;
namespace UIActivity.Models
{
    public class NewsArticle
    {
        public string image { get; set; }
        public string title{ get; set; }
        public int likes { get; set; }
        public int comments { get; set; }
        public bool is_liked { get; set; }
    }
}
